# Next-Technologies-Assignment




**Question **: Formally, a vector space V' is a subspace of a vector space V if
V' is a vector space
every element of V′ is also an element of V.
Note that ordered pairs of real numbers (a,b) a,b∈R form a vector space V. Which of the following is a subspace of V?
i) The set of pairs (a, a + 1) for all real a
ii) The set of pairs (a, b) for all real a ≥ b
iii) The set of pairs (a, 2a) for all real a
iv) The set of pairs (a, b) for all non-negative real a,b

**Answer **: 
**All the above 4 sentences are subspaces of V**

